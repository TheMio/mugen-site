---
layout: default
title: Nous contacter
language: fr
handle: /contact.html
---

![gif]({{ site.baseurl }}/images/pages/contacter.gif){: .center-image }

Si vous avez des questions, des demandes d'ordre général, n'hésitez pas.

Pour les problèmes, bogues et autres, il existe deux systèmes de gestion des *issues* de **Karaoke Mugen** :

- [Problèmes dans le logiciel Karaoke Mugen](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues)
- [Problèmes dans la base de karaokés](https://gitlab.com/karaokemugen/bases/karaokebase/issues)

Si vous rencontrez un souci, vérifiez avant qu'une *issue* n'existe pas déjà pour votre problème, sinon ouvrez-en une !

## Courriel

C'est simple : `mugen` arobase `karaokes.moe`

## Twitter

Ça aussi c'est simple : [@KaraokeMugen](https://twitter.com/karaokemugen).

## Mastodon

Suivez [@KaraokeMugen@shelter.moe](https://shelter.moe/@KaraokeMugen) !

## Discord

Pour nous trouver sur Discord, c'est sur ce [serveur](https://karaokes.moe/discord) !