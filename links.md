---
layout: default
title: Plus de karaoké !
language: fr
handle: /links.html
---

![gif]({{ site.baseurl }}/images/pages/liens.gif){: .center-image }

Il n'y a pas que Karaoke Mugen dans la vie : d'autres groupes font du karaoké ou même proposent librement leurs bases de données.

Vous connaissez d'autres entités qui font du karaoké ? [Contactez-nous]({{ site.baseurl }}/contact.html) pour qu'on les ajoute !

## Autres logiciels open source de karaoké

Les logiciels de karaoké *open source* ne manquent pas ! Si Karaoke Mugen ne répond pas à vos besoins, essayez de regarder parmi ceux-ci.

- [Karaoke Eternal](https://karaoke-eternal.com) : Un autre logiciel *open source* pour les fêtes et évènements, un peu comme Karaoke Mugen ! Sa grande différence est qu'il est plus modulaire et gère le format de karaoké CD+G. Il est aussi développé par un type super sympa.

- [Ultrastar Deluxe](https://github.com/UltraStar-Deluxe/USDX) : Le clone de SingStar par excellence, qu'on ne présente plus.

- [OpenKJ](https://openkj.org) : Un karaoké dans le *cloud* qui permet, entre autres de gérer des sessions karaoké où chacun vient chanter à son tour, de modifier le tempo, etc.

- [KaraKara](https://github.com/calaldees/KaraKara) : KaraKara est un autre système de karaoké pour les évènements type convention avec un bon nombre d'invités. Il propose un système complet de tags, de gestion de file d'attente et même un système de gestion de priorité des chanteurs !

- [Spivak](https://github.com/gyunaev/spivak) : Lecteur de karaoké simple avec une interface web pour gérer la file d'attente. Il lit beaucoup de formats de karaoké mais pas l'ASS !

- [Vocaluxe](https://vocaluxe.org) : Une alternative à Ultrastar assez jeune mais pleine de promesses avec notamment des modes dédiés aux fêtes !

- [Ponytone](https://ponytone.online) : Jeu karaoké multijoueurs en ligne sur des chansons de My Little Pony.

### Faire des karaokés

- [Aegisub Japan7](https://github.com/odrling/Aegisub) : Une version spéciale de [Aegisub](http://www.aegisub.org/) corrigeant quelques bugs et ajoutant quelques fonctionnalités dédiées au karaoké comme le *tap-to-time*.

## Autres amoureux du karaoké

### En France

- [Epitanime](https://www.epitanime.com/section-karaoke/) : L'association propose régulièrement des soirées karaoké dans l'enceinte de l'école, mais aussi durant la convention Epitanime qui a lieu tous les ans. Un rendez-vous à ne pas manquer si vous êtes fan de karaoké, l'ambiance est très souvent survoltée grâce à une expérience sans pareille des organisateurs sur les chansons à passer (ou non) pour animer une session de karaoké !
- [Bakaclub](http://manga.iiens.net/) : Bakaclub s'occupe de la [convention Bakanim](http://bakanim.iiens.net/) et dispose elle aussi d'une section karaoké dédiée. Vous trouverez chez Bakaclub de joyeux lurons qui n'hésiteront pas à vous donner un coup de main si vous avez des questions sur la création de karaokés !
- [CosPop](http://cospop.fr/) : Anciennement Bulle Japon, l'asso arpente les conventions en tant qu'association d'activités et le karaoké en fait partie. Leur catalogue est impressionnant !
- [ENSEEIHT-Japan7](https://www.bde.enseeiht.fr/clubs/japan7/karaoke/) : Les joyeux lurons de Japan7 ont grandement participé aussi à la base de Karaoke Mugen, mais aussi en réalisant quelques outils facilitant la création et la gestion des karaokés. Leur aide nous est très précieuse !
- Le club Japanim de l'ENSSAT : Située à Lannion, cette asso étudiante organise régulièrement des sessions karokés avec Karafun et leurs *times* maison.

### À l'international

- [Soramimi Karaoke](http://www.soramimi.nl/) : Soramimi Karaoke est un système complet proposant un logiciel et des karaokés dans leur propre format (heureusement facilement lisible.) Leur base de karaokés se compose essentiellement de versions longues de génériques et aussi de versions longues de J-Music.
- [KuroSensei's Anime Karaokes](https://github.com/KuroSensei/anime-karaokes-ass) : KuroSensei est un Chilien qui met à disposition ses karaokés d'animes au format .ASS sur Github !
- [Karaoke Kaizoku-dan](http://anime-karaoke.com/index.php) : La Brigade des Pirates du Karaoké sévit aux États-Unis en proposant du karaoké en convention et possède un bon répertoire. Vous pouvez les retrouver dans de nombreuses conventions majeures là-bas !