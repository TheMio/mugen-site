# Karaoke Mugen Website

If you want to propose new illustrations, do not hesitate to talk about it on [our Discord](https://karaokes.moe/discord) !

[Live website](https://mugen.karaokes.moe)
