---
layout: post
title:  "New release: Karaoke Mugen 2.5.2 !"
date:   2019-05-22 06:00:00
language: en
handle: /2019/05/22/KM252.html
---

Karaoke Mugen 2.5.2 "Konata 4-Koma" is out!

![gif]({{ site.baseurl }}/images/articles/1hare89.gif){: .center-image }

This is a bugfix release

# Downloads and changelog

- [Downloads]({{site.baseurl}}/en/download.html)
- [Changelog](https://gitlab.com/karaokemugen/code/karaokemugen-app/tags/v2.5.2)

If you have any questions or need help, please [contact us]({{site.baseurl}}/en/contact.html) !
