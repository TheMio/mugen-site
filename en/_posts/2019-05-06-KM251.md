---
layout: post
title:  "New release: Karaoke Mugen 2.5.1 !"
date:   2019-05-06 06:00:00
language: en
handle: /2019/05/06/KM251.html
---

Karaoke Mugen 2.5.1 "Konata Kiffante" is out!

This is a bugfix release.

![gif]({{ site.baseurl }}/images/articles/bb07943896b4bae2a54325ba8c0ccf74a46d8d6dr1-500-281_hq.gif){: .center-image }

# Downloads and changelog

- [Downloads]({{site.baseurl}}/en/download.html)
- [Changelog](https://gitlab.com/karaokemugen/code/karaokemugen-app/tags/v2.5.1)

If you have any questions or need help, please [contact us]({{site.baseurl}}/en/contact.html) !
