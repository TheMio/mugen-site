---
layout: post
title:  "Karaoke Mugen 4.1.7!"
date:   2020-08-17 06:00:00
language: en
handle: /2020/08/17/KM417.html
---

Karaoke Mugen 4.1.7 « Ougi Observatrice » stares at you intently. *jiiiii* !

![gif]({{ site.baseurl }}/images/articles/z1hr9h1rz9.gif){: .center-image }

This is a minor version but it has some new features and improvements !

# Downloads and changelog

- [Downloads]({{site.baseurl}}/en/download.html)
- [Changelog](https://gitlab.com/karaokemugen/code/karaokemugen-app/-/tags/v4.1.7)

If you have any questions or need help, please [contact us]({{site.baseurl}}/en/contact.html) !
