---
layout: post
title:  "New release: Karaoke Mugen 2.5.3 !"
date:   2019-06-30 06:00:00
language: en
handle: /2019/06/30/KM253.html
---

Karaoke Mugen 2.5.3 "Konata Kimono" is out!

![gif]({{ site.baseurl }}/images/articles/ozh8d32Dhr1tx45yjo1.gif){: .center-image }

This is a bugfix release

# Downloads and changelog

- [Downloads]({{site.baseurl}}/en/download.html)
- [Changelog](https://gitlab.com/karaokemugen/code/karaokemugen-app/tags/v2.5.3)

If you have any questions or need help, please [contact us]({{site.baseurl}}/en/contact.html) !
