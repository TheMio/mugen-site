---
layout: post
title:  "Karaoke Mugen 2.1 is here!"
date:   2018-04-18 10:00:00
language: en
handle: /2018/04/10/KM21.html
---

Karaoke Mugen 2.1 "Gabriel Glamoureuse" is available!

![gif]({{ site.baseurl }}/images/articles/mW1cgqEB.gif){: .center-image }

- [Downloads]({{site.baseurl}}/en/download.html)

# Before proceeding

- If your `userdata.sqlite3` file has nothing of value in it, please delete it before first launch. The migration process is supposed to work, but it's always better to start from a clean state.

- On first run, you'll be prompted to take a guided tour to create your first admin user. You'll need to complete this tour, but it doesn't take too long you'll see.

- There's been a lot of new features, and thus new settings. We advise you read  `config.ini.sample` and [the docs](https://docs.karaokes.moe/en/) to discover all the new innovative features and how to use them.

Thanks to Aeden, AxelTerizaki, Benoît Courtine, Kmeuh, Mirukyu, Spokeek and Ziassan for their work on this version!

# Changelog

See [Changelog](https://gitlab.com/karaokemugen/code/karaokemugen-app/tags/v2.1.0)

If you have any questions or need help, please [contact us]({{site.baseurl}}/en/contact.html) !
