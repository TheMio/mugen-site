---
layout: post
title:  "Introducing version 2.0-rc1"
date:   2017-10-24 17:00:00
language: en
handle: /2017/10/24/presentation_2.0rc1.html
---

Hello !

What about a quick tour around the changes coming with **Karaoke Mugen** version 2.0-rc1 ?

You sure dreamt of it, and here they come : greet the jingles ! Of course, you can use your own : put them in the `/app/jingles` folder and you're running. Default behavior will play a jingle every 20 videos, but you can tune that too.
If you want to look at ours, go to the [download page]({{ siteurl.baseurl }}/en/download.html).

![jingle]({{ site.baseurl }}/images/articles/jingle.gif)

Another addition, which might seem minor but how convenient : « go to top/bottom/current » buttons on the playlist, which you can find highlighted in red in the screenshot below. You can also notice the remaining time until playlist ends, highlighted blue.

![additions]({{ site.baseurl }}/images/articles/rc-1_1.jpg){: .max-image }

Would you like to cutomize the app further ? So what about choosing your own backgrounds ?! Yup, you'll find a `app/backgrounds` folder where you can drop multiple images. Feel free to burn your guest's retinas with your exquisite tastes.

![bg]({{ site.baseurl }}/images/articles/rc-1_2.jpg){: .max-image }

A few other options came in, such as the ability to cutomize the networking info message, or autoplaying a playlist as soon as first kara is added.

![infos]({{ site.baseurl }}/images/articles/rc-1_3.jpg){: .max-image }

And we're not done yet ! You can now look for a kara by author, get it playing right after the one playing by right-clicking the "add" button, or make the current playlist to repeat for an always more *infinite* karaoke !
Not to mention quite a few bugfixes, like *scrolling* on some devices or alternative name search which are back to their normal behaviours.

You will find here [a full changelog](https://gitlab.com/karaokemugen/code/karaokemugen-app/tags/v2.0-rc1) sur le dépôt Git de l'app.

* [Downlaod Karaoke Mugen 2.0-rc1]({{ siteurl.baseurl }}/en/download.html)
* [Karaoke Mugen 2.0-rc1 Source Code](https://gitlab.com/karaokemugen/code/karaokemugen-app/repository/v2.0-rc1/archive.zip)

For more detailed installation instructions, please reach the [complete documentation](https://docs.karaokes.moe/en/) !

Your feedback is important, and would allow us to fix any problem encountered or enhancing the application ! Don't hesitate to [contact us]({{ site.baseurl }}/en/contact.html) if you have any comment or issue !
