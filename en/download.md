---
layout: default
title: Download Karaoke Mugen
language: en
handle: /download.html
---

![gif]({{ site.baseurl }}/images/pages/telecharger.gif){: .center-image }

## Downloads

### Stable Versions

#### v{{ site.data.releases.channels.release.version }} « *{{ site.data.releases.channels.release.name }}* » ({{ site.data.releases.channels.release.date }})

##### Windows

- [Installer]({{ site.baseurl }}/downloads/Karaoke Mugen Setup {{ site.data.releases.channels.release.fileSuffix }}.exe) **recommended**
- [Portable .zip]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.release.fileSuffix }}-win.zip)

##### macOS

###### Intel Macs

- [.dmg Image]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.release.fileSuffix }}-mac-x64.dmg) **recommended**
- [Zip Archive]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.release.fileSuffix }}-mac-x64.zip)

###### Silicon (M1/M2/...) Macs

- [.dmg Image]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.release.fileSuffix }}-mac-arm64.dmg) **recommended**
- [Zip Archive]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.release.fileSuffix }}-mac-arm64.zip)

##### Linux

- [Flatpak](https://flathub.org/apps/details/moe.karaokes.mugen) **recommended**
- [AppImage x86]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.release.fileSuffix }}-linux-x86_64.AppImage)
- [AppImage arm64]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.release.fileSuffix }}-linux-arm64.AppImage)
- [ArchLinux](https://aur.archlinux.org/packages/karaokemugen/)
- [Ubuntu/Debian x86]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.release.fileSuffix }}-linux-amd64.deb)
- [Ubuntu/Debian arm64]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.release.fileSuffix }}-linux-arm64.deb)
- [Archive .tar.gz x86]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.release.fileSuffix }}-linux-x64.tar.gz)
- [Archive .tar.gz arm64]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.release.fileSuffix }}-linux-arm64.tar.gz)

###### Minimum requirements for Linux

**The Linux binaries require a GLIBC version 2.31 or higher**

For example for Ubuntu, this GLIBC is present in versions 20.04 and higher

## Installation

[Here is the installation documentation !](https://docs.karaokes.moe/en)

## Updates on Windows

### If you have an installation earlier than 3.2

Before the first installation via the installer, rename the folder where Karaoke Mugen is installed by `Karaoke Mugen`. When selecting the folder in the installer, select the parent folder. Karaoke Mugen will move into the renamed folder.

### If you want to update a portable version

If Karaoke Mugen is already downloaded and you'd like to update to a newer version, unpack the ZIP archive in the same folder and replace all old files by the new ones. **The portable version is not compatible with the automatic update.**

## Experimental Versions

Starting with version 4.x, these experimental branches are compatible with auto updates.

- `master` Version {{ site.data.releases.channels.master.version }} « *{{ site.data.releases.channels.master.name }}* » ({{ site.data.releases.channels.master.date }}) : bugfixes and small modifications
  - [Windows (Installer)]({{ site.baseurl }}/downloads/Karaoke Mugen Setup {{ site.data.releases.channels.master.fileSuffix }}.exe)
  - [Windows (Portable .zip)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.master.fileSuffix }}-win.zip)
  - [macOS (DMG Image for Intel Macs)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.master.fileSuffix }}-mac-x64.dmg)
  - [macOS (DMG Image for ARM (M1/M2) Macs)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.master.fileSuffix }}-mac-arm64.dmg)
  - [macOS (ZIP Archive for Intel Macs)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.master.fileSuffix }}-mac-x64.zip)
  - [macOS (ZIP Archive for ARM (M1/M2) Macs))]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.master.fileSuffix }}-mac-arm64.zip)
  - [Linux (AppImage x86)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.master.fileSuffix }}-linux-x86_64.AppImage)
  - [Linux (AppImage arm64)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.master.fileSuffix }}-linux-arm64.AppImage)
  - [Linux (AUR)](https://aur.archlinux.org/packages/karaokemugen-git/)
  - [Linux (.deb x86)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.master.fileSuffix }}-linux-amd64.deb)
  - [Linux (.deb arm64)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.master.fileSuffix }}-linux-arm64.deb)
  - [Linux (.tar.gz x86)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.master.fileSuffix }}-linux-x64.tar.gz)
  - [Linux (.tar.gz arm64)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.master.fileSuffix }}-linux-arm64.tar.gz)
- `next` Version {{ site.data.releases.channels.next.version }} « *{{ site.data.releases.channels.next.name }}* » ({{ site.data.releases.channels.next.date }}) : Development version, new features
  - [Windows (Installer)]({{ site.baseurl }}/downloads/Karaoke Mugen Setup {{ site.data.releases.channels.next.fileSuffix }}.exe)
  - [Windows (Portable)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.next.fileSuffix }}-win.zip)
  - [macOS (DMG Image for Intel Macs)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.next.fileSuffix }}-mac-x64.dmg)
  - [macOS (DMG Image for ARM (M1/M2) Macs)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.next.fileSuffix }}-mac-arm64.dmg)
  - [macOS (ZIP Archive for Intel Macs)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.next.fileSuffix }}-mac-x64.zip)
  - [macOS (ZIP Archive for ARM (M1/M2) Macs))]({{ site.baseurl }}/downloads/Karaoke Mugen-{{ site.data.releases.channels.next.fileSuffix }}-mac-arm64.zip)
  - [Linux (AppImage x86)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.next.fileSuffix }}-linux-x86_64.AppImage)
  - [Linux (AppImage arm64)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.next.fileSuffix }}-linux-arm64.AppImage)
  - [Linux (AUR)](https://aur.archlinux.org/packages/karaokemugen-git/)
  - [Linux (.deb x86)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.next.fileSuffix }}-linux-amd64.deb)
  - [Linux (.deb arm64)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.next.fileSuffix }}-linux-arm64.deb)
  - [Linux (.tar.gz x86)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.next.fileSuffix }}-linux-x64.tar.gz)
  - [Linux (.tar.gz arm64)]({{ site.baseurl }}/downloads/Karaoke Mugen-{{site.data.releases.channels.next.fileSuffix }}-linux-arm64.tar.gz)

## Source Code

Each branch and versions' sources can be reviewed on the [GIT repository](https://gitlab.com/karaokemugen/code/karaokemugen-app)

## Previous Versions

Here is the GIT page for [older versions](https://gitlab.com/karaokemugen/code/karaokemugen-app/tags).

Some old binary versions are still available [here](https://mugen.karaokes.moe/downloads).
