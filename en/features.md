---
layout: default
title: Features
language: en
handle: /features.html
---

![gif]({{ site.baseurl }}/images/pages/fonctionnalites.gif){: .center-image }

There are several ways you can enjoy group karaoke. Look at all those innovative features(tm) of Karaoke Mugen :

## A customized karaoke experience

### Karaoke management

- **Complete control over your playlists** : Add, remove, copy, shuffle, see how long it is and how much it'll last, import/export, fill from another list... everything's there!
  - **The current playlist** is the one the video player uses to play media
  - **The public list** is the one your public sends its suggestions to.
  - **Combine both lists** to get an autonomous karaoke where all songs added are played on screen!
- **Reorganise automatically your playlists.** You can drag and drop songs in the list, but also smart-shuffle it so it doesn't put all songs from the same guest at once, or doesn't put long songs together.
- Manage your users' **quota** : How many songs can they add? How long can they sing? Their quota is reset as soon as a song is played or if they're **freed** manually by the karaoke operator, by **likes** from other users, or automatically after a set amount of time.
- **Make the public vote!** Let them choose which song will go next via a poll. It's a great way to let your karaoke fill itself with the most liked songs from your users. You can thus join them and sing along!
- **Forbid people from adding songs already present,** may it be the exact same song or a song from the same series or singer.
- You can **hide** some songs in the playlist to make your karaoke more *mysterious* ! Mystery songs appear to users as entries with a ??? label in the playlist or can be made completely hidden to add even more surprise!
- Create **automixes** : Playlists made from your users' favorite songs!
- **Sessions** are karaoke moments which you can name and organise (conventions, weddings, parties, etc.) to gather stats about which songs were played or requested during said events.
- Some **jingles** can appear inbetween songs. We offer [a lot of jingles already](https://gitlab.com/karaokemugen/medias/jingles).
- Various **intros** can be displayed before every beginning of a playlist. [See them here](https://gitlab.com/karaokemugen/medias/intros).
- An advanced **blacklist system**, complete with automatic **criterias** and a **whitelist** allows you to hide particular songs, like ones for adults, italian songs or those sung by Nana Mizuki !
- Your karaoke ended but your guests still want some? **You can leave Karaoke Mugen to play random songs from your library** until someone adds a song to the list!
- Show off what you're thinking on **your Discord profile** so people can know which song you're destroying.

### User management

- Complete user profiles with nickname, avatar, etc.
- Users can have an online account to save their profile or favorites and find them again when they go to another Karaoke Mugen-powered event!
- Users and operators can choose **how to display series names**. *Attack on Titans* or *Shingeki no Kyojin*? *Fullmetal Alchemist* or *Hagane no Renkinjutsushi*?

### Online features

Karaoke Mugen can be used **without any internet connection**. However, with this feature your users would need to be **on the same Wi-Fi network** to use Karaoke Mugen.

Some optional features will need you to be connected to the internet though.

- Check for updates.
- **Download the songs you want** from various karaoke repositories. The [kara.moe repository](https://kara.moe) is added by default.
- **Online accounts** for your users :
  - A unique user profile so your users can log in from another Karaoke Mugen session.
  - Favorites can be stored online.
- A **shortened URL**, `xxxx.kara.moe` can be used to connect to your Karaoke Mugen session by your friends and guests. They can see the current playlist and suggest songs.
- Opt-in stats on your Karaoke Mugen usage can be sent to [Karaoke Mugen Server](https://kara.moe), if you give your consent.

### Media player

- The karaoke media can be controlled with ease: Play, pause, skip, previous, immediately stop, or delay stop after the current song ends, change volume... You can also temporarily hide lyrics to let your public sing by heart!
- The player's **volume** is adjusted automatically thanks to pre-computed audio gain data. Every song has a different volume, so this auto-adjust system won't scare you off when a loud song starts to play right after a quiet one.
- You can **choose which screen to use** but also have a second video player synced to the first one (also called **monitor**) to keep an eye of what's displayed on the main screen if you can't see it from where you are.
- Display **public announcement messages** on people's devices or on the big screen during a song. Messages such as *Happy Birthday!* or *Karaoke ends in one hour!*
- **Background wallpapers** can be completely modified if you'd like. You can even let Karaoke Mugen choose one at random among a list of files.

### Permissions

- The public's interface can be **opened** (by default), **limited** or **closed**. In limited mode, it only displays the current song and playlist, but doesn't let your guests add songs. This is useful when nearing the end of a karaoke session.
- You can forbid people from changing **nicknames**.
- You can even have **several blacklists** depending on when or where you are (weddings, anime conventions, birthdays, etc.)

## Karaoke Mugen in action

Public interface overview :

![img]({{ site.baseurl }}/images/features/invite.png){: .center-image }

Operator interface overview :

![img]({{ site.baseurl }}/images/features/admin.png){: .center-image }

Options overview :

![img]({{ site.baseurl }}/images/features/options.png){: .center-image }

How the karaoke looks like on the player's windows :

![img]({{ site.baseurl }}/images/features/mpv.png){: .center-image }

## Technical stuff

- Karaoke Mugen is software licensed under the [MIT license](https://en.wikipedia.org/wiki/Licence_MIT), a free software licence which allows to use the software as you see fit.
- Works on **Windows**, **Linux** and **macOS**.
- You can have **several folders** for your data. A folder for personal songs and one for the [Karaoke Mugen main repository](https://kara.moe), etc. These folders can even be on different filesystems or drives.
- All video formats supported by [mpv](http://mpv.io) are accepted.
- Subtitles are using the [ASS](https://en.wikipedia.org/wiki/SubStation_Alpha) format, convenient for karaokes.
- The main interface also works on smartphones and tablets.
- [A Websockets API](https://api.karaokes.moe/app/) so others can download new interfaces or clients.

## Roadmap

[Check out the roadmap](https://gitlab.com/karaokemugen/code/karaokemugen-app/-/milestones)  to know which features are planned next!

![gif]({{ site.baseurl }}/images/features/km2.jpg){: .center-image }
