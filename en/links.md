---
layout: default
title: More Karaoke!
language: en
handle: /links.html
---

![gif]({{ site.baseurl }}/images/pages/liens.gif){: .center-image }

Karaoke Mugen isn't the only group spreading the love for karaoke-- there are others too! Some of them even allow you to freely use their database like us.

Do you know other groups making and enjoying karaoke? [Contact us]({{ site.baseurl }}/en/contact.html) so we can add them!

## Other open source karaoke software

There's no shortage of open source karaoke software! If Karaoke Mugen doesn't suit your needs, try to look at those.

- [Karaoke Eternal](https://karaoke-eternal.com) : Another software made for parties and events, a little like Karaoke Mugen! Its big difference is its modularity and that it handles the CD+G karaoke format. The main developer is also awesome.

- [Ultrastar Deluxe](https://github.com/UltraStar-Deluxe/USDX) : The SingStar clone no one bothers to introduce anymore

- [OpenKJ](https://openkj.org) : A cloud-based karaoke solution which allows to manage karaoke sessions where people take turns singing, modify tempo, key, and other things.

- [KaraKara](https://github.com/calaldees/KaraKara) : KaraKara is another karaoke system for events like conventions with a good number of guests. It has a complete tag system, a queue manager with priority tokens and other fun stuff.

- [Spivak](https://github.com/gyunaev/spivak) : Standalone karaoke player with a simple web interface to manage the song queue. It can use a wide variety of karaoke formats (but not [ASS](https://en.wikipedia.org/wiki/SubStation_Alpha)!)

- [Vocaluxe](https://vocaluxe.org) : A young alternative to Ultrastar but with quite some promises, especially if you're looking for some fun party modes.

- [Ponytone](https://ponytone.online) : A multiplayer and online karaoke game specializing in My Little Pony songs.

### Create karaokes

- [Aegisub Japan7](https://github.com/odrling/Aegisub) : A special version of [Aegisub](http://www.aegisub.org/) fixing some bugs and adding some features like *tap-to-time*.

## Other karaoke lovers

### Worldwide

- [Soramimi Karaoke](http://www.soramimi.nl/) : Soramimi Karaoke is a complete system made of a software and a karaoke database in their own format (fortunately easy to read). Their karaoke database is mainly made of full-length versions of many anime and J-music songs.
- [KuroSensei's Anime Karaokes](https://github.com/KuroSensei/anime-karaokes-ass) : KuroSensei is a Chilean offering his karaokes to anyone on Github for animes from 2017 and 2018!

### United States

- [Karaoke Kaizoku-dan](http://anime-karaoke.com/index.php) : The Pirate Karaoke Brigade operates in the USA in many major anime events and has a great song catalog!

### France

- [Epitanime](https://www.epitanime.com/section-karaoke/) : This student/anime fan group offers karaoke nights at their school every week-end, as well as during the Epitanime Convention event every year. Great atmosphere and fun times await you there!
- [Bakaclub](http://manga.iiens.net/) : Another group managing the [Bakanim event](http://bakanim.iiens.net/) who has a dedicated karaoke team. You'll find fun and loving people at Bakaclub who will help you making karaoke if you have any questions!
- [Bulle Japon](http://www.bullejapon.fr/site/karaoke.php) : BJ for short, is offering activities and games at anime events in France. Karaoke happens to be one of them, and they have an impressive song catalog to offer to attendees.

