---
layout: default
title: Participate in expanding the Karaoke
language: en
handle: /participate.html
---

![gif]({{ site.baseurl }}/images/pages/participer.gif){: .center-image }

## The community of the Karaoke !

To join the community, ask question, get answers, and everything inbetween, there are two ways :

- [Our Discourse forum](https://discourse.karaokes.moe)
- [Our Discord server](https://karaokes.moe/discord)

Use whichever medium you prefer to chat with the community!

## Help Karaoke Mugen

Depending on what your skills are, and more importantly on what you'd like to do, we could need your help.

**Karaoke Mugen is a free and open-source project**, everyone can participate and bring its own stone to it. For you it could be a good opportunity for :

- Working with a group
- Learn how to master git, nodeJS, React, Aegisub, video encoding, HTML, CSS, JS...
- Put on your resume that you worked on a open-source project (some employers love it)
- Get to know new people
- Bring in your own skills and ideas
- **Most importantly, have fun and do soemthing together !**

Whatever your case is, if you have questions, don't hesitate and visit the [contact page]({{ site.baseurl }}/en/contact.html).

Here's a list of things on which we could need help with.

**NOTE : ** Even though the issues are in french for the most part, we know how to speak english and can provide you with more information if you're interested. We've been talking in french because so far we didn't really have any english-speaker working with us.

## The Karaoke database

- **Create your own karaoke songs** : Hearing new songs, being surprised by your choices (and your tastes), this is karaoke too! If you've got a song you'd really like to see sung in public or with your friends, or even just by yourself only, or if you've noticed the database lacks some Bleach endings (spoiler : it doesn't), [check out our tutorial on how to make karaoke!](https://docs.karaokes.moe/contrib-guide/create-karaoke)
- **Fix badly timed karaokes** : We have some in our database and it's no secret since we [list them whenever we find them](https://gitlab.com/karaokemugen/bases/karaokebase/issues?label_name%5B%5D=mal+tim%C3%A9) but we don't always have time to repair those quickly. You need to know how to use Aegisub.
- **Replacing videos with better quality ones** : A lot of our media are of standard or mediocre quality. We've made [an issue to follow what we did so far](https://gitlab.com/karaokemugen/bases/karaokebase/issues/370). We have a lot of videos in HD resolution or such which are ready for use, but it takes some time since we need to verify that karaokes are still synced with the new videos. It's a background task we do when we have free time, as there are a bit more than 300 low-resolution videos left! If you want to help, you should be familiar with video encoding and Aegisub.
- **We lack some info on a few karaoke songs** like their singer, songwriter, or their title. Sometimes it's from weird and old AMVs or MADs or other videos where we can't seem to find any info on. [The list of songs without information is kept updated here](https://gitlab.com/karaokemugen/bases/karaokebase/issues/417). You should know how to use a search engine or some good sites.
- **Jingles!** Nock, Nabashi and Stal already [made a few of them](https://gitlab.com/karaokemugen/medias/jingles) but if you'd like to make some new ones... These are shown inbetween songs during karaoke sessions, so help yourself if you'd like to contribute! You should know how to create videos.
- **Fullfill user requests!** We often get some [song requests](https://gitlab.com/karaokemugen/bases/karaokebase/issues?label_name%5B%5D=suggestion), but we don't always have time to do everything... If you have some time and want to *time* (or want to learn how to) create a karaoke file to make some people happy, come see us!

## Development

We have a few projects left and right which could need some help :

- **Work with AnimeOpenings**. We'd like to offer a way for people to quickly see what our karaokes are like via their browser. We made it so our database is synced with it but there are still a few bugs on [our live app](https://live.karaokes.moe) that we could need being ironed out, namely by fixing issues on [AnimeOpenings](https://github.com/AniDevTwitter/animeopenings) which we use for that part of the site or fix some issues on [the site's project on our lab](https://gitlab.com/karaokemugen/sites/live/issues). PHP/Javascript knowledge is required.
- **The main app has a bunch of issues that need some love**. [Check the list here](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues). Some of them involve SQL databases, others React and others just plain NodeJS, or even some black magic involving ffmpeg/mpv.
- **A Karaoke Mugen server is serving people songs**. It offers a URL shortener for your karaoke sessions, but also a karaoke file server so your app can download new songs easily, display stats or unified online accounts. [The project is already mature but still lacks some features](https://gitlab.com/karaokemugen/code/karaokemugen-server) You need to know how to use Nuxt/Vue, NodeJS and PostgreSQL.

## The Web part

- **The site web you're reading right now could be improved!** It uses [Jekyll](http://jekyllrb.com) for its generation, but apart from that it's plain HTML/CSS/JS. If you think you could make things look better or even help switch to a better static website generator, we're all ears! [The website's code and content can be found here.](https://gitlab.com/karaokemugen/sites/mugen-site)
- **Karaoke Mugen's documentation is done, but could also be better!** A better layout, a better translation, more examples, more help, more screenshots... [Sources are also available here!](https://gitlab.com/karaokemugen/sites/docs). We use [Hugo](https://gohugo.io/) because it's fairly simple, but there are surely better alternatives.
- **Twitter Game, Mastodon Game, Facebook Game...** That's all nice and fun but our presence on social networks is fairly minimal. Maybe we could do things better on that front. If you're used to these tools, or have some nice ideas, I'd love to hear about them.

## A better idea?

If you want to participate still but none of those topics get you excited, it's not a problem at all. If you have a great, big idea to offer and if you'd like to help make it real, [contact us!]({{ site.baseurl }}/en/contact.html)
