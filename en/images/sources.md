---
layout: default
title: Sources
language: en
handle: /images/sources
---

Here is a list of sources for the pictures posted on this website.

## Banner
**Sedeto** - [sedeto.fr](http://sedeto.fr)

## With Buddies
- [1]({{ site.baseurl }}/images/potes/potes1.jpg) - **La Traversée du Temps** - ©Madhouse
- [2]({{ site.baseurl }}/images/potes/potes2.jpg) - **La Traversée du Temps** - ©Madhouse
- [3]({{ site.baseurl }}/images/potes/potes3.jpg) - **La Traversée du Temps** - ©Madhouse
- [4]({{ site.baseurl }}/images/potes/potes4.jpg) - **NozokiAna** - ©Studio Fantasia
- [5]({{ site.baseurl }}/images/potes/potes5.jpg) - **Lucky Star** - ©Kyoto Animation
- [6]({{ site.baseurl }}/images/potes/potes6.jpg) - **Lucky Star** - ©Kyoto Animation
- [7]({{ site.baseurl }}/images/potes/potes7.jpg) - **Hibike! Euphonium** - [©Sawada Sanae](https://yande.re/post/show/374247)
- [8]({{ site.baseurl }}/images/potes/potes8.jpg) - **Vocaloid** - [©arieko](http://ecomimi.com/)
- [9]({{ site.baseurl }}/images/potes/potes9.jpg) - **Kill la Kill** - [©umakatsuhai](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=40102850)
- [10]({{ site.baseurl }}/images/potes/potes10.jpg) - **Imouto! Umaru-chan** - [©hijikini](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=52793398)
- [11]({{ site.baseurl }}/images/potes/potes11.jpg) - **Kantai Collection** - [©Yamato Tachibana](http://seiga.nicovideo.jp/seiga/im4202349)
- [12]({{ site.baseurl }}/images/potes/potes12.jpg) - **Kill la Kill** - [©azinori123](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=45304265)
- [13]({{ site.baseurl }}/images/potes/potes13.jpg) - **Precure!** - [©Yorudo Kaoru](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=43362819)

## With a public audience
- [1]({{ site.baseurl }}/images/public/public1.jpg) - **La Mélancholie de Haruhi Suzumiya** - ©Satelight
- [2]({{ site.baseurl }}/images/public/public2.jpg) - **La Mélancholie de Haruhi Suzumiya** - ©Satelight
- [3]({{ site.baseurl }}/images/public/public3.jpg) - **K-on!** - ©Kyoto Animation
- [4]({{ site.baseurl }}/images/public/public4.jpg) - **K-on!** - ©Kyoto Animation
- [5]({{ site.baseurl }}/images/public/public5.jpg) - **AKB0048** - ©Satelight
- [6]({{ site.baseurl }}/images/public/public6.jpg) - **Aibou** - [©madoka_(potechoco)](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=10021897)
- [7]({{ site.baseurl }}/images/public/public7.jpg) - **Touhou** - [©aramoneeze](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=34892413)
- [8]({{ site.baseurl }}/images/public/public8.jpg) - **Kill la Kill** - [©yosui](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=43914747)
- [9]({{ site.baseurl }}/images/public/public9.jpg) - **Naruto** - [©oba-min](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=44231171)
- [10]({{ site.baseurl }}/images/public/public10.jpg) - **Shingeki no Kyojin** - [©maedatomoko](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=42307077)

## Developed with passion
- [1]({{ site.baseurl }}/images/passion/passion1.jpg) - **Otaku no Video** - ©Gainax
- [2]({{ site.baseurl }}/images/passion/passion2.jpg) - **Otaku no Video** - ©Gainax
- [3]({{ site.baseurl }}/images/passion/passion3.jpg) - **Otaku no Video** - ©Gainax
- [4]({{ site.baseurl }}/images/passion/passion4.jpg) - **Tengen Toppa Gurren-Lagann** - ©Gainax
- [5]({{ site.baseurl }}/images/passion/passion5.jpg) - **Ensemble stars!** - [©kaminosaki1](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=62531305)
