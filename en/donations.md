---
layout: default
title: Donations
language: en
handle: /donations.html
---

In order to pay for the server that hosts us, as well as other expanses like certificates or developer accounts, we need your help. That's why we've prepared different ways for you to show your support. Choose the one you like best!

## Our wonderful and incredible donators

{% if site.data.hof.size > 0 %}
<h3>Idols de la grande scène</h3>
<p class="hof">
{% for fame in site.data.hof %}
<div>
  <img src="{{ fame.image }}" style="width: 200px;">
  <div>{{ fame.name }}</div>
</div>
{% endfor %}
</p>

These idols are those who subscribed to the Karaoke Master tier on our Patreon, but all of the people who donate are awesome, everyone contribute its part to the Karaoke Mugen infrastructure and development:
{% else %}
Here are listed all our regular Patreon donators.
{% endif %}

<ul class="columns" data-columns="2">
{% for donator in site.data.donators %}
  <li>
    {{ donator }}
  </li>
{% endfor %}
</ul>

Thanks to the {{ site.data.hof.size | plus: site.data.donators.size }} Karaoke Mugen donators!

We are also very grateful to all of you who donated once via other means like Paypal or Liberapay.

## How to donate

**Note:** We don't accept any kind of cryptocurrency.

### Patreon

<a href="https://www.patreon.com/bePatron?u=64513113" data-patreon-widget-type="become-patron-button">{{ site.f_patreon[page.language] }}</a><script async src="https://c6.patreon.com/becomePatronButton.bundle.js"></script>

### Paypal

<form action="https://www.paypal.com/donate" method="post" target="_top">
	<input type="hidden" name="hosted_button_id" value="2FGU9X24PPGFS" />
    <input type="image" src="https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Bouton Faites un don avec PayPal" />
    <img alt="" border="0" src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" width="1" height="1" />
</form>

If the button doesn't work, [try this link](https://www.paypal.com/donate/?hosted_button_id=YTZKDQWQS5R4J).

### Liberapay

<script src="https://liberapay.com/karaokemugen/widgets/button.js"></script>
<noscript><a href="https://liberapay.com/karaokemugen/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>


## Any question?

If you have questions on how donations to Karaoke Mugen work, [contact us!](/contact.html)
