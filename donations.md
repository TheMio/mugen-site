---
layout: default
title: Donations
language: fr
handle: /donations.html
---

Afin de financer le serveur sur lequel nous sommes, ainsi que d'autres choses, nous avons besoin de votre aide. C'est pourquoi nous avons mis en place différentes façons pour vous de nous envoyer un peu d'argent. Choisissez celle qui vous convient le mieux !

![gif]({{ site.baseurl }}/images/pages/donation.gif){: .center-image }

## Nos merveilleux et incroyables donateurs

{% if site.data.hof.size > 0 %}
<h3>Idols de la grande scène</h3>
<p class="hof">
{% for fame in site.data.hof %}
<div>
  <img src="{{ fame.image }}" style="width: 200px;">
  <div>{{ fame.name }}</div>
</div>
{% endfor %}
</p>

Ces idols sont ceux qui sont Karaoke Master sur notre page Patreon, mais tous les gens qui donnent sont géniaux, tout le monde contribue de sa part à l'infrastructure et au développement de Karaoke Mugen :
{% else %}
Ici sont listés les donateurs réguliers via Patreon.
{% endif %}

<ul class="columns" data-columns="2">
{% for donator in site.data.donators %}
  <li>
    {{ donator }}
  </li>
{% endfor %}
</ul>

Merci aux {{ site.data.hof.size | plus: site.data.donators.size }} donateurs de Karaoke Mugen !

Nous remercions également chaleureusement tous ceux qui ont bien voulu faire un don via les autres moyens à disposition (Paypal ou Liberapay)

## Comment donner

**Note :** Nous n'acceptons pas les dons en cryptomonnaies.

### Patreon

<a href="https://www.patreon.com/bePatron?u=64513113" data-patreon-widget-type="become-patron-button">{{ site.f_patreon[page.language] }}</a><script async src="https://c6.patreon.com/becomePatronButton.bundle.js"></script>

### Paypal

<form action="https://www.paypal.com/donate" method="post" target="_top">
	<input type="hidden" name="hosted_button_id" value="2FGU9X24PPGFS" />
    <input type="image" src="https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Bouton Faites un don avec PayPal" />
    <img alt="" border="0" src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" width="1" height="1" />
</form>

Si jamais le bouton ne fonctionne pas, [essayez ce lien](https://www.paypal.com/donate/?hosted_button_id=2FGU9X24PPGFS).

### Liberapay

<script src="https://liberapay.com/karaokemugen/widgets/button.js"></script>
<noscript><a href="https://liberapay.com/karaokemugen/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>

### Des questions?

Vous vous posez plein de questions existentielles sur les donations à Karaoke Mugen ? [Contactez-nous !](/contact.html)
