---
layout: default
title: Fonctionnalités
language: fr
handle: /features.html
---

![gif]({{ site.baseurl }}/images/pages/fonctionnalites.gif){: .center-image }

Il y a plusieurs façons d'apprécier le karaoké en groupe. Laissez-vous séduire par les fonctionnalités innovantes(tm) de Karaoke Mugen :

## Une expérience personnalisée du karaoké

### Gestion du karaoké

- **Gestion complète des listes de lecture** : Ajout, suppression, copie, mélange, affichage de la durée totale et restante, import/export, remplissage depuis une autre liste... Tout y est !
  - **La liste courante** est celle utilisée par le lecteur.
  - **La liste publique** est celle dans laquelle votre public envoie ses suggestions.
  - **Combinez les deux listes** pour un karaoké autonome où toutes les chansons ajoutées passeront à l'écran !
- **Réorganisez automatiquement la liste de lecture intelligemment.** En maintenant un karaoké avec la souris, vous pouvez mélanger la liste de lecture en fonction de la longueur des pistes (éviter deux chansons trop longues à la suite) ou bien éviter qu'une personne soit favorisée et voie toutes ses demandes passer en même temps.
- **Faites voter votre public !** Laissez le public choisir quelle chanson passera ensuite parmi une sélection depuis la liste de suggestions. Idéal également pour laisser un karaoké se piloter automatiquement. Comme ça, vous pourrez rejoindre le public et chanter vous aussi !
- **Interdisez ou non les doublons,** que ça soit la même chanson ou une chanson de la même série ou du même chanteur.
- Gérez **le quota** de vos utilisateurs : combien de chansons ont-ils le droit d’ajouter ? De combien de temps de chant disposent-ils ? Le quota est réinitialisé au fur et à mesure que les chansons d’un utilisateur passent, ou, si elles sont **libérées** manuellement par l’opérateur, par les **likes** des autres utilisateurs, ou automatiquement après un certain temps d’attente (réglable).
- Vous pouvez **cacher** certaines chansons dans la liste de lecture pour rendre votre karaoké plus *mystérieux* ! Les chansons mystères peuvent apparaître sous forme d'entrée dans la liste de lecture ou être complètement invisibles pour encore plus de surprises.
- Créez un **automix** : une liste de lecture faite à partir des favoris de vos utilisateurs présents !
- Les **sessions** vous permettent de nommer vos soirées ou évènements de karaoké (conventions, mariages, etc.) afin d'en tirer des statistiques plus tard. Elles sont exportables.
- Des **jingles** peuvent s'intercaler entre un nombre défini de chansons. Nous proposons [déjà de nombreux jingles](https://gitlab.com/karaokemugen/medias/jingles).
- Des **intros** peuvent être lues avant chaque démarrage de playlist. Le dépôt [est ici](https://gitlab.com/karaokemugen/medias/intros).
- Un système avancé de **critères** de **liste noire** et de **liste blanche** vous permet de cacher certaines chansons, comme celles réservées aux adultes, les chansons en italien ou celles chantées par Nana Mizuki !
- Votre karaoké est fini mais vos invités en veulent encore ? **Vous pouvez laisser le hasard choisir des chansons de votre bibliothèque !**. La fête ne s'arrête jamais !
- Affichez ce que vous chantez sur **votre profil Discord** ! Comme ça vos amis sauront quelle chanson vous massacrez.

### Gestion des utilisateurs

- Profil complet avec pseudo, avatar, etc.
- Les utilisateurs et l'opérateur peuvent choisir **comment afficher le nom des séries**. *Duck Tales* ou *La Bande à Picsou* ? *L'attaque des Titans* ou *Shingeki no Kyojin* ? *Fullmetal Alchemist* ou *Hagane no Renkinju-* nan, sérieusement ?
- Ajout ou retrait de chansons d'une liste de **favoris** par utilisateur, afin de rapidement retrouver ses chansons préférées et les proposer.

### Fonctionnalités en ligne

Karaoke Mugen peut être utilisé **sans connexion Internet**. Pour que vos utilisateurs puissent se connecter, **ils doivent être sur le même réseau WiFi que l'ordinateur faisant tourner Karaoke Mugen**.

Certaines fonctionnalités optionnelles vous demanderont d'être connecté à internet :

- Vérification si une **mise à jour de Karaoke Mugen est disponible**
- **Téléchargez les chansons que vous voulez** et ce depuis différents dépôts de chansons Karaoke Mugen. [Le dépôt Kara.moe](https://kara.moe/base) est activé par défaut.
- **Comptes en ligne** pour vos utilisateurs :
  - Profil unique et en ligne pour se connecter depuis une autre session de Karaoke Mugen.
  - Favoris unifiés et stockés en ligne.
- Une **URL raccourcie**, `xxxx.kara.moe` est utilisable pour que vos invités puissent accéder à votre interface et suggérer des chansons, consulter la playlist...
- Des statistiques sur l'utilisation de Karaoke Mugen (consultables quand on aura une page pour ça) peuvent être envoyées à [Karaoke Mugen Server](https://kara.moe), si vous donnez votre accord.

### Lecteur vidéo

- La vidéo du karaoké peut être manipulée à loisir : **Lecture**, **Pause**, **Chanson suivante** ou **précédente**, **Arrêt immédiat** ou **Arrêt après la chanson en cours**, **Ajustement du volume**... Vous pouvez même **Cacher les paroles** temporairement pour laisser votre public chanter par cœur !
- Le **volume de lecture** est ajusté automatiquement grâce au calcul du *gain audio* entre chaque chanson. Chaque vidéo ayant un volume différent, ça évite d'avoir à monter ou baisser le volume manuellement à chaque changement de chanson !
- Vous pouvez **choisir quel écran utiliser**, mais aussi avoir un deuxième lecteur vidéo affiché sur votre propre écran (appelé **moniteur**) pour garder un œil sur le karaoké quand vous n'avez pas une vue sur l'écran principal.
- Affichez des **messages** d'annonce directement sur les appareils de vos utilisateurs ou sur le grand écran pendant une chanson. Par exemple : *Joyeux anniversaire !* ou *Le karaoké se termine dans une heure !*
- Affichage (ou non) de **l'avatar de celui qui a demandé une chanson**.
- Les **fonds d'écran** peuvent être complètement modifiés si vous le souhaitez. Un fond d'écran peut même être choisi aléatoirement à chaque pause si plusieurs fichiers sont présents dans le dossier.

### Permissions

- L'interface peut être **ouverte** (par défaut), **limitée** ou **fermée**. En mode **limité**, l'interface n'affiche que la chanson en cours et la playlist, et ne permet plus d'ajouter de chanson. Utile quand vous souhaitez limiter l'interaction avec les utilisateurs ou voulez les empêcher d'ajouter de nouvelles chansons !
- Vous pouvez interdire aux gens de **changer de pseudo**.
- Vous pouvez même **avoir plusieurs listes noires** selon les endroits et les moments (mariages, conventions otaku, etc.)

## En image

Aperçu de l'interface invité. À gauche la vue mobile, à droite la vue PC / tablette.

![img]({{ site.baseurl }}/images/features/invite.png){: .center-image }

Aperçu de la gestion des playlists dans l'interface administrateur :

![img]({{ site.baseurl }}/images/features/admin.png){: .center-image }

Aperçu de la gestion des options dans l'interface administrateur :

![img]({{ site.baseurl }}/images/features/options.png){: .center-image }

Visuel de l'écran principal où la vidéo est diffusée, avec les informations de base : nom du karaoké, nom de la version de **KM**, pseudo du demandeur...

![img]({{ site.baseurl }}/images/features/mpv.jpg){: .center-image }

## Détails techniques

- Karaoke Mugen est un logiciel sous licence [MIT](https://fr.wikipedia.org/wiki/Licence_MIT), une licence libre qui vous permet d'utiliser l'application comme bon vous semble.
- Fonctionne sous **Windows**, **Linux** et **macOS**.
- Possibilité d'avoir **différents dossiers** pour ses karaokés. Un dossier pour les karas personnels et un pour ceux de [la Base Karaoke Mugen](https://kara.moe), etc.. Ces dossiers peuvent être même **sur des disques durs différents** de l'application Karaoke Mugen.
- Prise en charge de tous les formats vidéos possibles par [mpv](https://mpv.io).
- Utilisation d'ASS, un format de sous-titrage adapté aux karaokés.
- Une interface pour smartphone/tablette et ordinateur~~, compatible IE6~~.
- [Une API Websockets](https://api.karaokes.moe/app) pour le développement d'autres interfaces ou de clients mobiles.

## Feuille de route

[Consultez la feuille de route](https://gitlab.com/karaokemugen/code/karaokemugen-app/-/milestones) pour connaître les prochaines fonctionnalités innovantes(tm) qui seront développées !