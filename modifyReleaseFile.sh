echo "VersionNumber: $VERSIONNUMBER"
echo "VersionName: $VERSIONNAME"
echo "Release: $RELEASE"

if [ "$RELEASE" != "" ]
then
	VERSION=`echo $VERSIONNUMBER | awk -F- {'print $1'} `
	DATE=`date +"%Y-%m-%d"`
	jq --arg vdate "$DATE" --arg vnum "$VERSION" --arg vfile "$VERSIONNUMBER" --arg vname "$VERSIONNAME" --arg rel "$RELEASE" '.channels[$rel] = {version: $vnum, name: $vname, fileSuffix: $vfile, date: $vdate}' releases.json >_data/releases.json
else
	cp -f releases.json _data/releases.json
fi
