---
layout: post
title:  "Les versions de Karaoke Mugen"
date:   2017-11-14 10:00:00
language: fr
handle: /2017/11/14/km_versions.html
---

Nous avons revu la page de téléchargement pour afficher les liens par système d'exploitation plutôt que par version.

Aussi, deux nouvelles versions font leur apparition, nous allons vous expliquer à quoi elles correspondent :

- Les versions numérotées (2.0, 2.0 beta, etc.) sont des versions **stables** et qui fonctionnent normalement bien. Ce sont les versions que nous recommandons d'utiliser.

- La version `master` est mise à jour à chaque version de Karaoke Mugen crée par nos soins. Elle peut être en avance de quelques corrections par rapport à la dernière version numérotée disponible. C'est la branche « C'est stable mais on est pas super sûrs quand même. »

- La version `next` est l'expérimentale, le mode nightmare, le défi ultime : il s'agit de versions de Karaoke Mugen qui ont des fonctionnalités encore non réellement testées, possiblement des bugs, mais ça vous permet d'avoir un avant goût des nouveautés. Par exemple dans la version `next` à l'heure où j'écris ces lignes, on peut prévisualiser les 15 premières secondes d'un kara avant de le selectionner, histoire d'être sûrs d'avoir la bonne musique !

`master` et `next` sont mises à jour quotidiennement. Vous devrez retélécharger le tout à chaque fois, par contre. Si vous tenez réellement à rester à jour, privilégiez l'utilisation de git.
