---
layout: post
title:  "Karaoke Mugen à Japan Expo Sud"
date:   2018-03-05 10:00:00
language: fr
handle: /2018/03/05/japan_expo_sud.html
---

Juste une petite news pour dire que [Forum Thalie](http://forum-thalie.fr) sera à [Japan Expo Sud](http://www.japan-expo-sud.com/fr/) pour y tenir un stand d'activités. Et parmi celles-ci se trouve du karaoké, qui sera géré grâce à **Karaoke Mugen**.

C'est la première fois que **Karaoke Mugen** sera utilisé en convention. Même s'il s'agit de la version 2.0.7 qui est plutôt stable et éprouvée, c'est une petite victoire pour le projet.

N'hésitez pas à allez les voir sur le stand **P092** ! Ils ont plein d'activités pour vous détendre, en plus du karaoké ! Plus d'infos sur [ce tweet](https://twitter.com/ForumThalie/status/969527486189989888)
