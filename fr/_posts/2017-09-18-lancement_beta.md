---
layout: post
title:  "Lancement de la beta !"
date:   2017-09-18 11:16:44
language: fr
handle: /2017/09/18/release_2.0beta.html
---

La première version beta de **Karaoke Mugen 2.0** est disponible !

![alélouya]({{ site.baseurl }}/images/articles/viochl.gif){: .center-image }

[Téléchargement Karaoke Mugen 2.0 Beta](https://gitlab.com/karaokemugen/code/karaokemugen-app/repository/v2.0-beta/archive.zip)

Cette version, baptisée "Finé Flegmatique" propose pratiquement toutes les [fonctionnalités décrites sur le site]({{ site.baseurl }}/features.html).

Pour l'installer, rendez-vous sur la [documentation complète](https://docs.karaokes.moe/fr) pour des infos plus détaillées !

Vos retours sont importants, cela permet de régler les problèmes et d'améliorer le tout : [contactez-nous !]({{ site.baseurl }}/contact.html) Si vous rencontrez un souci, cette page est aussi faite pour vous !

# Problèmes connus

- Sur certains ordinateurs, la génération de la base de données peut prendre un certain temps. Les ordinateurs les plus lents / vieux et avec leur base de karaokes sur un disque dur lent peuvent atteindre les 20 minutes. Sur un i7 avec un disque dur mécanique, cela atteint les 3 ou 4 minutes. Avec un SSD, ce temps est divisé par deux. N'hésitez pas à nous faire des retours, pour ça, lors de la génération de la base, repérez la ligne

```
AddKara durationMs=xxxx
```

où xxxx est la durée qu'a pris **Karaoke Mugen** pour ajouter vos karaokes

- Pour cette version beta, l'installation se fait comme tout projet nodeJS via `npm install`, cela ne sera pas nécessaire pour les betas suivantes où nous proposerons des executables à télécharger.
