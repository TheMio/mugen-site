---
layout: post
title:  "Faire un don à Karaoke Mugen"
date:   2021-11-16 06:00:00
language: fr
handle: /2021/11/16/donations.html
---

On nous l'a parfois demandé, mais jusqu'ici nous n'avions pas besoin de recevoir des dons des gens qui aiment Karaoke Mugen. L'argent nous intéresse comme tout le monde, mais nous faisons cela pour la passion avant tout, et si l'équipe principale de Karaoke Mugen n'a que quelques personnes à son actif, le nombre de gens qui participent de près ou de loin au projet se comptent en plusieurs dizaines.

Merci à vous tous, sans vous on en serait pas là aujourd'hui.

![gif]({{ site.baseurl }}/images/articles/1hr8z9.gif){: .center-image }

Ceci étant dit, Karaoke Mugen est pour le moment hébergé sur Shelter, un petit serveur communautaire qui aujourd'hui a beaucoup de mal à gérer tout ce qu'il a, et Karaoke Mugen mange une bonne partie de ses ressources. On a donc pris plusieurs décisions :

- Se séparer du [Lab de Shelter](http://lab.shelter.moe) pour héberger nos dépôts git chez [Gitlab.com](https://gitlab.com) directement. Heureusement ils ont un programme Open Source qui va nous permettre de bénéficier de toutes leurs fonctionnalités sans débourser un sou.
- S'auto-héberger sur notre propre serveur afin de ne plus déranger Shelter.

Si le premier point ne nécessite aucun investissement financier, pour le second on a pas trop le choix et il va falloir louer un serveur à part.

La question d'accepter les dons d'argent devient du coup d'actualité.

## Pourquoi donner ?

Comme dit ci-dessus la gestion de l'infrastructure et du logiciel est un peu complexe et demande des ressources, donc il nous faut un hébergement dédié. Cela coûte entre 50 et 100€ par mois selon ce qu'on prendra et des donations que l'on recevra.

Bien sûr, qui dit donations libres sans limite veut dire aussi qu'on a des plans possibles pour utiliser cet argent. Il peut servir à, pêle-mêle :

- Imprimer des flyers à distribuer en convention
- Obtenir un compte développeur Apple pour signer l'application KM pour macOS et peut-être même en créer une pour iOS.
- Placer des primes sur certains bugs ou certaines fonctionnalitéss qu'on arrive pas à développer nous-mêmes
- Commissionner plus de dessins de [Sedeto](http://sedeto.fr), qui a fait le design de notre logo et de Nanamin.
- et peut-être d'autres idées plus tard ?

Votre argent pourra servir à cela et sera sur un compte Paypal dédié.

## Comment donner ?

Merci d'avoir lu jusqu'ici.

On propose trois façons de donner, que vous pouvez retrouver ici :

### Patreon

<a href="https://www.patreon.com/bePatron?u=64513113" data-patreon-widget-type="become-patron-button">{{ site.f_patreon[page.language] }}</a><script async src="https://c6.patreon.com/becomePatronButton.bundle.js"></script>

### Paypal

<form action="https://www.paypal.com/donate" method="post" target="_top">
	<input type="hidden" name="hosted_button_id" value="2FGU9X24PPGFS" />
    <input type="image" src="https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Bouton Faites un don avec PayPal" />
    <img alt="" border="0" src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" width="1" height="1" />
</form>

Si jamais le bouton ne fonctionne pas, [essayez ce lien](https://www.paypal.com/donate/?hosted_button_id=2FGU9X24PPGFS).

### Liberapay

<script src="https://liberapay.com/karaokemugen/widgets/button.js"></script>
<noscript><a href="https://liberapay.com/karaokemugen/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>

## Pourquoi 3 façons de donner ?

On s'est dit que certains aimaient Patreon parce que c'est pratique, d'autres n'aiment pas mais ont un compte Paypal, et d'autres sont encore plus rebelles et préfèrent un système comme Liberapay. Notez que les trois moyens de paiement arriveront de toutes façons sur un compte Paypal. Cependant ils vous permettront d'utiliser une carte bancaire plutôt que votre compte Paypal quoi qu'il arrive.

Il y a des contreparties côté Patreon, parce que c'est plus simple, mais nous ne pourrons pas en offrir à ceux faisant des dons Paypal ou Liberapay car cela sera plus difficile à gérer. Pour rappel, nous ne sommes que très peu à gérer Karaoke Mugen et notre temps libre est ainsi limité. On essaye donc de concilier notre vie et Karaoke Mugen. Ce qu'on ne fait pas en gestion, on le fait en codant et en améliorant le logiciel et les services autour.

Merci de votre compréhension :)

Si jamais vous avez des questions, n'hésitez pas à nous [contacter](/contact.html). Et si vous voulez aider autrement, [on prend aussi !](/participate.html)
