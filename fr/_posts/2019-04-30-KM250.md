---
layout: post
title:  "Karaoke Mugen 2.5.0 !"
date:   2019-04-30 06:00:00
language: fr
handle: /2019/04/30/KM250.html
---

Karaoke Mugen 2.5.0 « Konata Karaokiste » sort du garage !

![gif]({{ site.baseurl }}/images/articles/ngioaezb.gif){: .center-image }

Il s'agit d'une version majeure, avec énormément de nouveautés que nous allons décrire en détail. Merci de lire ce billet attentivement.

## Nouveautés importantes

### Nouvelle base de données « PostgreSQL »

Jusqu'ici nous utilisions une base de données SQLite pour que Karaoke Mugen gère ses données. C'était une bonne idée pour une utilisation simple avec très peu d'utilisateurs simultanés.

En 2018 lors de la convention Jonetsu 3.33 nous avons constaté que SQLite était insuffisant et faisait beaucoup trop ramer l'application lorsque plus de 15 personnes accédaient simultanément à l'interface.

Cela nous aura donc pris un moment pour migrer vers ce nouveau moteur de base de données PostgreSQL mais maintenant c'est fait.

Qu'est-ce que cela change pour vous ?

À priori rien : Karaoke Mugen s'occupe de lancer et couper PostgreSQL lors de son lancement. Si vous avez déjà un serveur PostgreSQL chez vous que vous souhaiteriez utiliser, le fichier de configuration de Karaoke Mugen est modifiable en ce sens : reportez-vous au [nouveau site de documentation](https://docs.karaokes.moe/fr) pour cela.

Le changement le plus visible sera une rapidité accrue lors du chargement de données comme des listes de karaokés. En interne cela nous a aussi permis quelques optimisations bienvenues.

C'est un grand jalon que nous avons atteint avec cette migration, car elle permet également d'uniformiser le code de Karaoke Mugen et de [Karaoke Mugen Server](https://kara.moe)

Au démarrage, Karaoke Mugen va importer les données de votre ancienne base SQLite et renommer celle-ci en fin de traîtement.

### Nouveau fichier de configuration

Le fichier `config.ini` a disparu au profit de `config.yml`. Le [format YAML](https://fr.wikipedia.org/wiki/YAML) est plus adapté pour une configuration hiérarchisée que le format INI à plat.

Au premier démarrage de Karaoke Mugen 2.5.0, votre configuration INI sera importée et sauvegardée au format YAML.

### Gestionnaire de téléchargements (BETA)

On nous l'a beaucoup demandé : vous pouvez maintenant télécharger des karaokés individuellement ou par lot depuis une interface dédiée dans le panneau Système de Karaoke Mugen.

Cela vous permet d'essayer Karaoke Mugen avec uniquement les chansons de votre choix : vous n'avez plus besoin de télécharger l'intégralité de la base de karaokés pour apprécier le logiciel.

Cette fonctionnalité est encore en BETA et va nécessiter beaucoup d'améliorations que nous allons faire au fur et à mesure des versions 2.5.x de Karaoke Mugen. N'hésitez pas à [nous faire vos retours]({{site.baseurl}}/contact.html) à ce sujet !

Vous pouvez néanmoins toujours mettre à jour intégralement la base comme avant dans le menu Database.

### Comptes en ligne

Autre fonctionnalité beaucoup demandée, la possibilité d'avoir un compte en ligne.

Lors de la création de votre compte vous pouvez choisir d'utiliser le serveur Karaoke Mugen de votre choix (par défaut `kara.moe` mais ça peut être aussi celui de votre association par exemple) pour avoir votre compte en ligne. Par exemple, je suis Axel, lorsque je vais créer mon compte sur mon application Karaoke Mugen, j'indiquerai `Axel` et dans l'autre case je laisserai `kara.moe`. Un compte en ligne, synchronisé avec celui de mon application, sera crée.

Ce compte pour le moment permet de garder votre avatar, vos informations, et vos favoris synchronisés. Cela veut dire que vous pouvez aller chez quelqu'un d'autre, sur son app Karaoke Mugen à lui, et vous connecter avec votre compte en ligne plutôt que d'en créer un propre pour récupérer vos favoris chez l'instance de votre ami.

Si vous avez déjà un compte local sur votre application Karaoke Mugen, vous pouvez le convertir en compte en ligne depuis votre profil.

Dans le respect de votre vie privée et poru être conforme à la RGPD (on espère) vous pouvez à tout moment supprimer votre compte en ligne, toujours depuis votre profil. Dans ce cas, le compte redevient local uniquement sur votre application.

Vous pouvez aussi complètement désactiver les comptes en ligne sur votre application pour empêcher leur création/utilisation sur votre application (par exemple si vous n'êtes pas connecté à Internet)

### Autres nouveautés

Consultez la liste détaillée ci-dessous pour voir les autres nouveautés.

## Mise à jour

Rien à signaler : récupérez la dernière version et décompressez-là dans le dossier de votre installation actuelle de Karaoke Mugen.

Si vous avez des questions, n'hésitez pas à [nous soliciter]({{ site.baseurl }}/contact.html)

## Téléchargements

Comme d'hab, [direction la page de téléchargements]({{site.baseurl}}/download.html)

## Changements

### Nouvelles fonctionnalités innovantes

- [#339](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/339) Les karaokés peuvent être téléchargés individuellement depuis un serveur Karaoke Mugen (comme `kara.moe`) au lieu que la base doive être téléchargée dans son entièreté. Allez dans le menu Karas -> Download du panneau système. Cette fonctionnalité est encore en BETA.
- [#303](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/303) Les utilisateurs peuvent maintenant créer des comptes en ligne ce qui veut dire que leurs favoris et infos de profil sont stockés en ligne et plus uniquement sur l'application Karaoke Mugen locale. Les comptes en ligne sont activés par défaut.
- [#382](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/382) Ajout du tag COVER pour les chansons de ce type.
- [#393](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/393) Ajout du tag DRAMA pour les chansons issues de feuilletons japonais ou coréens
- Ajout du tag Wii U pour les chansons venant de jeux Nintendo Wii U.
- [#418](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/418) Ajout du tag FANDUB pour les vidéos de doublage (avec bande rythmo)
- [#377](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/377) Déjà présent depuis la 2.4.1 : les stats d'utilisation sont envoyés vers Karaoke Mugen Server périodiquement (si l'admin de l'instance est OK)
- [#392](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/392) Les opérateurs de karaoké peuvent ajouter plusieurs karaokés aléatoires à la liste de lecture courante avec un bouton dans l'interface de gestion. Par exemple pour remplir une liste de 10 karas aléatoires.

### Améliorations

- [#404](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/404) Quand l'interface est en mode restreint, un modal s'affiche pour expliquer à l'utilisateur qu'il ne peut plus ajouter de chansons pour le moment.
- [#355](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/355) Le stockage de la configuration se fait maintenant dans un fichier YML plutôt que INI. Votre configuration INI sera convertie automatiquement au démarrage.
- [#389](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/389) Les favoris sont stockés de façon plus intelligente en base de données. Vous pouvez supprimer sans souci toute playlist de favoris une fois passé sur la version 2.5.0
- [#379](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/379) Karaoke Mugen utilise désormais une base de données PostgreSQL au lieu de SQLite3. Cela simplifie énormément le code de l'application et permet des temps de réponse beaucoup plus courts.
- [#375](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/375) Déjà présent depuis la 2.4.1 : les petites citations d'initialisation sont maintenant affichées sur l'écran de bienvenue
- [#349](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/349) La lecture de MP3 est désormais plus dynamique, avec des effets de visualisation.
- [#283](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/283) L'avatar de celui ou celle qui a demandé une chanson s'affiche désormais à côté de l'info de la chanson lors de la lecture à l'écran.
- Les groupes de téléchargement est maintenant un critère de filtrage / blacklist.
- De nouveaux noms de comptes invités et des catchphrases !
- Les transitions entre les chansons sont maintenant plus courtes car le fond d'écran du karaoké n'est pas rechargé à chaque fois.
- La liste noire est regénérée dorénavant après chaque génération de base de données pour que ça reste cohérent.
- Nouvelle option de ligne de commande `--noBaseCheck` pour désactiver la vérification des données (quand vous êtes certain que rien n'a changé).
- Nouvelle option de ligne de commande `--reset` pour réinitialiser vos données utilisateur. ATTENTION : cela supprime utilisateurs, stats, playlists, etc.
- La configuration n'est plus mise à jour en temps réel si vous modifiez le fichier de configuration pendant que Karaoke Mugen tourne car ça causait des soucis. Vous devrez modifier le fichier de configuration avec Karaoke Mugen éteint pour que vos modifications prennent effet au prochain lancement.
- Toutes les communications avec Karaoke Mugen Server sont maintenant en HTTPS.
- Le fichier executable a vu sa taille grandement réduite en remplaçant certains packages avec des versions plus simples et légères.
- La génération des vidéos de prévisualisation a été réécrite et devrait être plus cohérente.
- Lors de la création d'un karaoké, les vidéos MP4 seront web-optimisées automatiquement.
- [#388](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/388) Les opérateurs de karaoké peuvent ajouter plusieurs karaokés aléatoires à la liste de lecture courante avec un bouton dans l'interface de gestion. Par exemple pour remplir une liste de 10 karas aléatoires.

### Correctifs

- [#387](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/387) La recherche de chansons prend maintenant en compte les alias de séries.
- [#386](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/386) Karaoke Mugen ne permet plus de mettre des virgules dans les noms de séries.
- [#385](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/385) Karaoke Mugen ne vous ajoute plus en tant qu'auteur d'un karaoké que vous éditez s'il n'y a pas d'autre auteur inscrit.
- [#384](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/384) Le nom de la série est maitnenant traduit selon la langue du navigateur de l'utilisateur dans le panneau système.
- Le listage des fonds d'écran ne prend plus en compte les fichiers n'étant pas des images en compte dans le dossier `app/background`. Cela faisait crasher mpv.
- [#399](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/399) Le bouton de suppression de kara dans la liste de lecture n'est plus planqué par l'interface publique sur mobile.
- [#415](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/415) Les invités ne voient plus le bouton des favoris.
- Direct3D n'est plus le driver de sortie vidéo par défaut de mpv sous Windows.