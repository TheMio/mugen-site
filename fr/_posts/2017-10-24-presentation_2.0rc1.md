---
layout: post
title:  "Présentation de la 2.0-rc1"
date:   2017-10-24 17:00:00
language: fr
handle: /2017/10/24/introducing_2.0rc1.html
---

Bonjour !

Que diriez-vous de faire un rapide tour des changements apportés avec la version 2.0-rc1 de **Karaoke Mugen** ?

Eh oui, vous en rêviez, les jingles font leur apparition ! Bien entendu, vous pouvez utiliser les vôtres, il suffit pour cela de les placer dans le répertoire `/app/jingles` et le tour est joué. L'intervalle par défaut est d'un jingle toutes les 20 vidéos, mais il est possible de le modifier.
Si vous voulez récupérer les nôtres, rendez-vous [sur la page de téléchargement]({{ siteurl.baseurl }}/download.html).

![jingle]({{ site.baseurl }}/images/articles/jingle.gif)

Autre ajout qui peut sembler anecdotique, mais ô combien pratique, les boutons « aller en haut/bas de liste » ainsi qu'« aller à la piste courante » en rouge sur la capture d'écran ci-dessous, ainsi que l'ajout du temps restant avant la fin de la playlist en cours, en bleu.

![ajout]({{ site.baseurl }}/images/articles/rc-1_1.jpg){: .max-image }

Vous aimeriez personnaliser un peu plus l'appli ? Alors que dites-vous de choisir vous-même, non pas votre, mais **vos** *backgrounds* ?! Un dossier `app/backgrounds` fait ainsi son apparition. Vous pouvez donc brûler la rétine de vos invités avec vos goûts discutables.

![bg]({{ site.baseurl }}/images/articles/rc-1_2.jpg){: .max-image }

Notons également que de nouvelles options font leur arrivée, en particulier la possibilité de personnaliser le message des infos du réseau, ainsi que de lire automatiquement la playlist, dès l'ajout du premier kara.

![infos]({{ site.baseurl }}/images/articles/rc-1_3.jpg){: .max-image }

Et ce n'est pas tout ! Vous pouvez désormais chercher les karas d'un auteur précis, lire un kara tout de suite après celui en cours, grâce à un clic droit sur le bouton d'ajout ou encore la possibilité de répéter automatiquement la playlist courante, pour des karaokes toujours plus *infini* ! N'oublions pas non plus les résolutions de certains bugs, comme le *scrolling* sur certains supports ou le retour à un fonctionnement normal de la recherche des noms alternatifs.

Vous pouvez [consulter la liste de les toutes modifications](https://gitlab.com/karaokemugen/code/karaokemugen-app/tags/v2.0-rc1) sur le dépôt Git de l'app.

* [Téléchargement de Karaoke Mugen 2.0-rc1]({{ siteurl.baseurl }}/download.html)
* [Code source de Karaoke Mugen 2.0-rc1](https://gitlab.com/karaokemugen/code/karaokemugen-app/repository/v2.0-rc1/archive.zip)

Pour l'installer, rendez-vous sur la [documentation complète](https://docs.karaokes.moe/fr/) pour des infos plus détaillées !

Vos retours sont importants, cela permet de régler les problèmes et d'améliorer le projet : [contactez-nous !]({{ site.baseurl }}/contact.html)
