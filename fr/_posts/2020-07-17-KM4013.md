---
layout: post
title:  "Karaoke Mugen 4.0.13 !"
date:   2020-07-17 06:00:00
language: fr
handle: /2020/07/17/KM4013.html
---

Karaoke Mugen 4.0.13 « Ôgi Obscure » va vous éblouir !

![gif]({{ site.baseurl }}/images/articles/oun98bhlM11wt5xqro1.gif){: .center-image }

C'est la version la plus aboutie de Karaoke Mugen à ce jour (non sans blague).

## Téléchargements

Téléchargez la dernière version sur [la page de téléchargements.]({{site.baseurl}}/download.html)

## Mettre à jour votre installation

### Windows

- Si vous choisissez la version par installeur, vous n'avez qu'à l'exécuter et pointer vers le chemin de votre choix.
- Si vous choisissez la version portable, sachez que **les mises à jour auto de l'application ne seront pas possibles depuis celle-ci**
  - Décompressez simplement l'archive dans votre dossier Karaoke Mugen et lancez l'exécutable possédant une icône Karaoke Mugen. L'ancien exécutable peut être supprimé sans souci.

### macOS

Pour macOS, installez l'image DMG comme n'importe quelle application Mac. Pour ouvrir l'application, vous devrez faire clic droit puis « Ouvrir » dans le menu. L'application n'étant pas signée (nous n'avons pas les moyens de cela), macOS Catalina vous le fera savoir, à vous de nous faire confiance (ou non).

Si vous déposez Karaoke Mugen dans le dossier Applications, Karaoke Mugen cherchera ses données dans votre dossier utilisateur. Si c'est dans un autre dossier (celui de votre ancienne installation par exemple) assurez-vous qu'un fichier `portable` existe, sans extension. Ainsi Karaoke Mugen cherchera ses fichiers de données dans ce dossier à la place.

### Linux

Pour Linux, des instructions spécifiques sont disponibles dans la documentation, [merci de la consulter](https://docs.karaokes.moe).

## Nouveautés de cette version

### Changement de numérotation de version

La numérotation des versions a changé :

- Le numéro de version majeure (ici 4) changera à chaque fois que l'on sortira une version avec de nouvelles fonctionnalités. On changera également le nom du personnage de la version.
- Le numéro de version intermédiaire (ici 0) changera à chaque fois que l'on sortira une version stable et officielle de Karaoke Mugen. Elle contiendra des bugfixes et de petites améliorations par rapport à la version précédente, mais il n'y aura pas de grands changements. Cela changera l'adjectif du nom de la version.
- Les versions mineures peuvent changer plus souvent, seulement pour les gens sur les branches `master` et `next` de Karaoke Mugen. Il y aura une version mineure chaque dimanche de façon automatique s'il y a eu des modifications apportées au code depuis la dernière version.

### Nouvelles fonctionnalités innovantes(tm)

- Le mode public/privé a disparu. À partir de maintenant, les playlists peuvent avoir à la fois l'état « courant » et « public ».
  - Pour reproduire l'ancien comportement du mode « public » il faut avoir deux playlists, l'une avec l'état courant et l'autre avec l'état public. La liste « publique » recevra les suggestions et la liste « courante » sera celle jouée.
  - Pour reproduire l'ancien comportement du mode « privé », il faut qu'une seule playlist ait les deux états simultanément. Ainsi, les ajouts du public iront dedans et seront directement joués.
- Vous pouvez jouer des chansons directement depuis la bibliothèque sans avoir à les ajouter à une playlist.
  - Si vous faites cela en pleine lecture de la liste courante, celle-ci reprendra à la fin de la chanson que vous aurez mis en lecture depuis la bibliothèque.
- Après la fin d'une playlist, Karaoke Mugen peut jouer des chansons aléatoirement en attendant que quelqu'un ajoute une nouvelle chanson à la playlist (un peu comme un « attract mode »).

![img]({{ site.baseurl }}/images/articles/random kara.png)

- Les playlists créées avec Karaoke Mugen Live ou une autre app Karaoke Mugen peuvent être importées depuis le gestionnaire de téléchargement pour récupérer toutes les chansons qu'elle contient si vous ne les avez pas déjà.
- Les séries sont maintenant des tags. Cela n'impacte pas l'utilisateur final, mais nous a permis de nettoyer plus de 1500 lignes de code puisque les séries et les tags étaient traités séparément alors qu'ils étaient finalement le même genre d'information en rapport avec une chanson. Cela devrait également améliorer la vitesse des recherches.
- Lorsque vous visualisez une playlist différente de la courante et que vous appuyez sur le bouton Play, un warning s'affichera pour vous signaler que la playlist que vous regardez n'est pas celle qui sera jouée.
- Des trucs et astuces seront maintenant affichés durant la phase d'initialisation de l'application.
- Nous utilisons désormais [Sentry.io](https://sentry.io) pour récupérer les rapports d'erreur détaillés lorsque vous rencontrez un souci avec le logiciel. Cela nous permet de traiter des bugs en amont sans que vous ailliez besoin de les signaler. Karaoke Mugen vous demandera si vous acceptez d'envoyer ces rapports. Par défaut, c'est désactivé, puisque certains rapports d'erreur peuvent contenir des données permettant de vous identifier.
- La génération de la base de données durant la phase d'initialisation a maintenant une barre de progression.
- Les groupes de karaokés sont mieux affichés sur le gestionnaire de téléchargement pour que vous puissiez plus facilement les trouver et les ajouter à la file d'attente.
- Dans le menu de sélection, les playlists ont maintenant des icônes selon leur type.

![img]({{ site.baseurl }}/images/articles/icon playlists.png)

- Karaoke Mugen peut désormais gérer plusieurs types de fichiers si vous les glissez et déposez dans la fenêtre ou si vous associez Karaoke Mugen au sein de votre système d'exploitation :
  - Les fichiers `.kara.json` seront joués automatiquement s'ils existent dans votre base de données.
  - Les fichiers `.karabundle` sont des packages de karaokés qui peuvent être ajoutés à votre base de données. Vous pouvez les télécharger puis le site web d'un dépôt (comme kara.moe par exemple).
  - Les fichiers `.kmplaylist` sont des playlists exportées par une application Karaoke Mugen ou Karaoke Mugen Live. Si certains karaokés ne sont pas dans votre base de données, ils seront téléchargés.
  - Les fichiers `.kmfavorites` ajouteront des karaokés à votre liste de favoris.
  - Les fichiers `.kmblcset` ajouteront des règles de critères de blacklist à votre application (voir ci-dessous).
- Ajout de règles de critères de blacklist : vous pouvez maintenant créer différentes collections de critères de blacklist et passer de l'un à l'autre selon l'endroit où vous êtes (entre amis, marriage, à une convention otak/geek, etc.)
- Le code de sécurité peut maintenant être copié dans le presse-papier.
- La popup des détails de chanson peut être fermée en cliquant ailleurs à l'écran, ou avec le bouton « échap ».
- Ajout d'un outil dans le panneau système pour comparer deux dépôts et vérifier si les paroles sont différentes entre les deux, et permettre de les mettre à jour facilement. Cela va aider les mainteneurs de la base qui gèrent plusieurs dépôts.
- Ajout d'un outil dans le panneau système pour montrer les tags avec le même nom mais avec différents types, qui pourront ensuite être fusionnés en un seul tag.
- Ajout d'un outil d'édition de masse dans le panneau système pour ajouter/retirer des tags spécifiques à une liste de chansons préalablement créée.
- Une sauvegarde de la base de données est faite automatiquement au démarrage, vous permettant de la restaurer en cas de problème. Cela nous permettra aussi de mettre à jour la base plus tard.
- Une file d'attente a été ajoutée pour les actions lourdes sur la base de données comme la génération et les rafraichissements, ce qui évitera des comportements bizarres quand plusieurs ont lieu en même temps.
- Ajout d'un bouton de validation dans la page de la base de données pour valider uniquement les fichiers changés depuis la dernière génération.
- Un bouton `détails` est disponible pour chaque chanson du gestionnaire de téléchargements.
- On a changé d'outil de migration de la base (db-migrate vers postgrator), nous permettant de déclencher des actions spécifiques selon les mises à jour à faire.
- Tous les messages du panneau système sont maintenant traduits en français (et en anglais bien sûr).
- L'accélération matérielle est activée par défaut dans un mode automatique sans échec, permettant de décoder de façon matérielle les vidéos dans de nombreux cas, économisant la batterie de vos appareils portables. Vous pouvez le désactiver complètement si cela cause des soucis sur certaines vidéos.
- Un nouveau paramètre vous permet d'ajouter des options supplémentaires au lecteur vidéo. Pour les utilisateurs avancés uniquement.
- L'interface publique vous permet maintenant de plus facilement voter pour une chanson déjà présente dans la playlist publique lorsque vous parcourez la bibliothèque.

![img]({{ site.baseurl }}/images/articles/public delete upvote.png)

- Vous pouvez maintenant utiliser les touches multimédia de votre clavier s'il en a (stop, précédent, suivant, play/pause) pour contrôler Karaoke Mugen (pas sur macOS à cause d'un bug de Electron 9.x).
- Prise en charge du profil Discord. Vous pouvez maintenant afficher fièrement ce sur quoi vous chantez sur votre profil Discord !

![img]({{ site.baseurl }}/images/articles/discord.png)

#### Interface opérateur revue et simplifiée

  - Moins de notifications inutiles (comme les ajouts et les suppressions de kara).
  - Suppression du bouton pour afficher les infos d'un kara. Un clic direct sur le kara souhaité suffit maintenant.
  - Les options ont été déplacées dans le bouton **K** en haut à droite.
  - Le bouton « stop » arrêtera le lecteur après la chanson en cours. Une deuxième pression arrêtera le lecteur immédiatement.
  - L'option de changement de mode de l'interface publique a été déplacé dans les raccourcis en haut à gauche.
  - Le temps écoulé et restant d'une chanson sont affichés de part et d'autre dans la barre de progression.

![img]({{ site.baseurl }}/images/articles/time remaining.png)

- Les jingles et sponsors apparaissent désormais sous la forme d'étiquettes dans la playlist courante afin que l'opérateur puisse voir quand est-ce qu'ils seront joués.

![img]({{ site.baseurl }}/images/articles/jingles sponsors playlist.png)

- Les options avancées de karaoké et de playlist sont maintenant dans des menus contextuels, symbolisés par une icône de clé à molette. Terminé la barre d'icônes incompréhensibles qui se rajoute sur l'interface.

![img]({{ site.baseurl }}/images/articles/advanced tools.png)

- Les filtres peuvent être réinitialisés.
- Les boutons précédent et suivant sont maintenant grisés si vous êtes au début ou en fin de playlist. Le bouton play est également grisé s'il n'y a rien dans la playlist courante.
- Les paramètres spécifiques à l'application ont été retirés des options opérateur et sont désormais dans le panneau système.
- Des options ont été simplifiées ou retirées.

![img]({{ site.baseurl }}/images/articles/new admin 4.0.gif)

### Améliorations

- Les mots de passe sont maintenant stockés d'une façon plus sûre (en utilisant un algorithme bcrypt salé plutôt que du SHA256)
  - Pour améliorer la sécurité de votre compte, changer votre mot de passe sur KM Server ou une autre application KM vous déconnectera automatiquement, car maintenant KM Server stocke la dernière fois que votre mot de passe a été modifié.
- Les permissions d'un utilisateur sont désormais vérifiées à chaque appel API, pour que les utilisateurs ayant perdu leur statut d'admin ne puissent plus s'en servir.
- Migration du panneau système du framework antd v3 à v4, ce qui donne du code plus propre et de meilleurs visuels.
- La taille totale des dépôts configurés est maintenant affiché sur le gestionnaire de téléchargements.
- L'installeur va arrêter de tout effacer dans le dossier `app` lors d'une mise à jour.
- Les groupes sont maintenant affichés en tant que cases à cocher dans le formulaire d'édition/d'ajout de karaoké.
- Les karaokés d'anime et de jeux vidéo sont maintenant mieux différenciés dans le panneau système.
- Pour des raisons de debug, les appels à Internet sont mieux enregistrés.
- Lorsque que l'on télécharge/met à jour toutes les chansons d'un dépôt, une notification apparaîtra pour montrer ce qui est en train de se passer.
- Le mode sombre du panneau système est désormais effectif partout.
- L'autocomplétion des chanteurs et série est un peu plus rapide sur le panneau système.
- Le code SQL est désormais mieux organisé dans le code source de l'app.
- Lorsqu'on change la priorité d'un dépôt (le déplacer dans la liste), un timer validera votre changement et régénérera votre base de données.
- Le module servant à dialoguer avec mpv a été entièrement réécrit.
- La phase d'initialisation a été entièrement revue lorsqu'on affiche les logs.
- Les tests unitaires sont démarrés à la fin du démarrage de l'app, accélérant le cycle d'intégration continue.
- Les codes d'erreur d'API ont été normalisés. Karaoke Mugen peut être une théière maintenant.
- Les logs sont un peu mieux gérés (et ils sentent bon).

### Correctifs

- Correction des blocages mortels de la base de données dans certaines situations.
- On arrête de publier votre IP trop souvent au raccourcisseur d'URL.
- Les longs fichiers de paroles s'affichent mieux dans la page d'information des karas.
- Désormais, le fait de créer un karaoké pour le dépôt A avec un tag existant uniquement dans le dépôt B créé également un tag dans le dépôt A.
- Créer un karaoké avec 3x le même tag (qui n'existait pas auparavant) ou plus fonctionne désormais.
- La liste des playlists est désormais correctement rafraichie lors du login.
- Sélectionner des dossiers devrait mieux fonctionner désormais.
- Correction d'un crash de l'appli lorsque KM Server est désactivé.
- Correction de l'échec d'import des fichiers MKV.
- Correction du changement de dossier primaire d'un dépôt.
- Correction de la page de détails d'un kara qui avait un `#` dans son nom.

Pour plus de détails, [consultez le changelog complet (en anglais)](https://gitlab.com/karaokemugen/code/karaokemugen-app/-/releases)
