---
layout: post
title:  "Developpement en cours"
date:   2017-12-12 10:00:00
language: fr
handle: /2017/12/12/dev_state.html
---

Le temps passe vite, cela fait déjà un mois que nous avions fait un billet sur le développement de **Karaoke Mugen** !

J'espère que vous vous amusez bien avec le logiciel et sa base.

Nous continuons à l'améliorer, mais nous n'avons pas donné beaucoup de nouvelles. Ce post est là pour y remédier.

![gif]({{ site.baseurl }}/images/articles/841213561.gif){: .center-image }

# Correctifs

La version actuelle de **Karaoke Mugen** est la **2.0.5**. Elle corrige des bugs mineurs à droite à gauche. Pour suivre les dernières versions stables, n'hésitez pas à consulter [la page des versions](https://gitlab.com/karaokemugen/code/karaokemugen-app/tags) sur le Lab. Entre autres, des bugs liés aux jingles et à la génération de la base de données ont été corrigés.

# La branche `next`

`next` est la version de **Karaoke Mugen** stable mais qui contient les dernières nouveautés reservées à la version **2.1** « Gabriel Glamoureuse ». Cette version n'est pas recommandée pour une utilisation courante, mais vous pouvez à tout moment l'essayer quand même. La seule fonctionnalité notable ajoutée pour le moment est la prévisualisation des vidéos du karaoke depuis l'interface web, pour être certain d'avoir la bonne chanson avant de l'ajouter !

# Quoi d'autre ?

Nous sommes en train de travailler d'arrache-pied sur deux chantiers assez gros :

- Directement visible, une gestion des utilisateurs. Nous reviendrons dessus en temps et en heure mais cela prend du temps car nous découvrons au fur et à mesure comment implémenter ça et il faut penser à tous les cas de figure, permettre à l'administrateur de créer des comptes à l'avance et aux utilisateurs eux-mêmes de créer leurs propres comptes, avoir un avatar, etc. Cela peut paraître inutile, mais la gestion des utilisateurs est comme une fondation pour d'autres fonctionnalités innovantes (erm) comme [un système de favoris](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/199), [permettre de *like* une suggestion du public](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/167), [un mode « vote du public »](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/79) ou encore [donner une note à un karaoke](https://gitlab.com/karaokemugen/code/karaokemugen-app/issues/22).
- Moins visible mais tout aussi important, nous sommes en train de réécrire **Karaoke Mugen** en **ES2015+**. C'est une norme de Javascript qui est plus lisible et moins verbeuse que celle employée actuellement. Du code plus lisible permet de travailler plus efficacement, de dénicher des bugs plus facilement et d'ajouter des nouvelles fonctions plus rapidement. Déjà sur `next` une partie du code existe en **ES2015+** mais il reste encore énormément à faire. Cela permettra aussi de faciliter l'arrivée de nouveaux développeurs !
- [Côté base](https://gitlab.com/karaokemugen/bases/karaokebase), nous avons dépassé les 5000 karaokes ! Nous avons passé un petit moment à développer un système d'intégration continue, pour valider chaque changement de la base. Ainsi, on est sûrs d'avoir une base en accord avec les vidéos.

Je vous rappelle d'ailleurs que **Karaoke Mugen** existe car je voulais me mettre à développer en **node.js**. Avoir un projet de ce genre, à la fois ludique et qui sert à d'autres personnes, est très motivant pour apprendre un nouveau langage. Si vous n'y connaissez rien en node, mais que vous voulez aider et vous y mettre, alors [venez nous voir sur Discord !](https://karaokes.moe/discord) On vous aidera à vous y mettre.

N'attendez pas la version **2.1** avant un petit moment encore : je m'en vais au Japon entre le 26 Décembre et le 19 Janvier. Par conséquent, à moins que mes collègues Ziassan et Benoît Courtine ne carburent de leur côté et fassent mon boulot à ma place, il va s'écouler encore quelques temps.

Bon karaokes à vous tous, et passez de bonnes fêtes !
